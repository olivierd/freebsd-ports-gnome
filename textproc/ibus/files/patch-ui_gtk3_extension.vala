--- ui/gtk3/extension.vala.orig	2020-02-21 12:43:01 UTC
+++ ui/gtk3/extension.vala
@@ -67,7 +67,7 @@ class ExtensionGtk : Gtk.Application {
 
 
     private void bus_name_acquired_cb(DBusConnection connection,
-                                      string sender_name,
+                                      string? sender_name,
                                       string object_path,
                                       string interface_name,
                                       string signal_name,
@@ -78,7 +78,7 @@ class ExtensionGtk : Gtk.Application {
     }
 
     private void bus_name_lost_cb(DBusConnection connection,
-                                  string sender_name,
+                                  string? sender_name,
                                   string object_path,
                                   string interface_name,
                                   string signal_name,
