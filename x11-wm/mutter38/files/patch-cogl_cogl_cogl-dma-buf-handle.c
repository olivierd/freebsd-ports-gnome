--- cogl/cogl/cogl-dma-buf-handle.c.orig	2020-09-14 19:40:40 UTC
+++ cogl/cogl/cogl-dma-buf-handle.c
@@ -36,10 +36,20 @@
 
 #include <errno.h>
 #include <gio/gio.h>
-#include <linux/dma-buf.h>
 #include <sys/ioctl.h>
 #include <sys/mman.h>
 #include <unistd.h>
+
+#define DMA_BUF_SYNC_READ  (1 << 0)
+#define DMA_BUF_SYNC_WRITE (2 << 0)
+#define DMA_BUF_SYNC_RW    (DMA_BUF_SYNC_READ | DMA_BUF_SYNC_WRITE)
+#define DMA_BUF_SYNC_START (0 << 2)
+#define DMA_BUF_SYNC_END   (1 << 2)
+#define DMA_BUF_IOCTL_SYNC _IOW ('b', 0, struct dma_buf_sync)
+struct dma_buf_sync
+{
+  uint64_t flags;
+};
 
 struct _CoglDmaBufHandle
 {
