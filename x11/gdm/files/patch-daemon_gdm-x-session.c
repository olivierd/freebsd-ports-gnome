--- daemon/gdm-x-session.c.orig	2020-12-15 18:17:04 UTC
+++ daemon/gdm-x-session.c
@@ -114,7 +114,8 @@ prepare_auth_file (void)
         GError   *error = NULL;
         gboolean  prepared = FALSE;
         Xauth     auth_entry = { 0 };
-        char      localhost[HOST_NAME_MAX + 1] = "";
+        size_t    name_max = sysconf (_SC_HOST_NAME_MAX);
+        char      localhost[name_max + 1];
 
         g_debug ("Preparing auth file for X server");
 
@@ -124,7 +125,7 @@ prepare_auth_file (void)
                 return NULL;
         }
 
-        if (gethostname (localhost, HOST_NAME_MAX) < 0) {
+        if (gethostname (localhost, name_max) < 0) {
                 strncpy (localhost, "localhost", sizeof (localhost) - 1);
         }
 
