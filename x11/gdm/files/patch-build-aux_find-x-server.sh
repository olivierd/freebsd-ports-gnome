--- build-aux/find-x-server.sh.orig	2020-12-15 18:17:03 UTC
+++ build-aux/find-x-server.sh
@@ -29,6 +29,8 @@ elif test -x /usr/openwin/bin/Xsun; then
     echo "/usr/openwin/bin/Xsun"
 elif test -x /opt/X11R6/bin/X; then
     echo "/opt/X11R6/bin/X"
+elif test -x /usr/local/bin/Xorg; then
+    echo "/usr/local/bin/Xorg"
 else
     echo ""
 fi
