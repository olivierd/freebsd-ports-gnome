--- daemon/gdm-display-access-file.c.orig	2020-12-15 18:17:04 UTC
+++ daemon/gdm-display-access-file.c
@@ -337,9 +337,16 @@ _create_xauth_file_for_user (const char  *username,
         g_debug ("GdmDisplayAccessFile: creating %s", auth_filename);
         /* mode 00600 */
         errno = 0;
-        fd = g_open (auth_filename,
-                     O_RDWR | O_CREAT | O_EXCL | O_BINARY,
-                     S_IRUSR | S_IWUSR);
+        /* It is reserved UID for 'gdm' user, register in ports/UIDs */
+        if (uid == 92) {
+                fd = g_open (auth_filename,
+                             O_RDWR | O_CREAT | O_EXCL | O_BINARY,
+                             S_IRUSR | S_IWUSR | S_IRGRP);
+        } else {
+                fd = g_open (auth_filename,
+                             O_RDWR | O_CREAT | O_EXCL | O_BINARY,
+                             S_IRUSR | S_IWUSR);
+        }
 
         if (fd < 0) {
                 g_set_error (error,
@@ -441,9 +448,11 @@ _get_auth_info_for_display (GdmDisplayAccessFile *file
                  *
                  * https://bugs.freedesktop.org/show_bug.cgi?id=43425
                  */
-                char localhost[HOST_NAME_MAX + 1] = "";
+                size_t name_max = sysconf (_SC_HOST_NAME_MAX);
+                char localhost[name_max + 1];
+
                 *family = FamilyLocal;
-                if (gethostname (localhost, HOST_NAME_MAX) == 0) {
+                if (gethostname (localhost, name_max) == 0) {
                         *address = g_strdup (localhost);
                 } else {
                         *address = g_strdup ("localhost");
